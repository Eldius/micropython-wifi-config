#!/bin/bash

function reset_device {
    #ampy -p $1 -b 115200 reset
    echo "Doing nothing..."
}

device_list=[]

for device in /dev/ttyUSB*
do
    echo "working on device: ${device}"
    #reset_device ${device}
    #sleep 10
    device_folder_name=$( rshell --quiet -p ${device} -b 115200 boards | awk '{print $1}' )
    #reset_device ${device}
    echo "device name: ${device_folder_name}"
    sleep 2
    rshell -p ${device} -b 115200 rsync ./src/ /${device_folder_name}
done
