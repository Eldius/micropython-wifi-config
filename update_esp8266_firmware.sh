#!/bin/bash

#esp8266-20190529-v1.11.bin
#esp8266-20190821-v1.11-231-g96ace8082.bin

esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect 0 bin/esp8266-20190821-v1.11-231-g96ace8082.bin && \
    notify-send --urgency=low "hey you!" "Succes!" || \
    notify-send --urgency=critical "hey you!" "Oops"
