# Tests using MicroPython with ESP32 and ESP8266 #

## TIPS #

Access serial port without `sudo`:

    sudo usermod -a -G dialout $USER

List connected boards:

    rshell -p /dev/ttyUSB0 -b 115200 boards

## Links ##

- https://nodemcu.readthedocs.io/en/master/modules/wifi/
