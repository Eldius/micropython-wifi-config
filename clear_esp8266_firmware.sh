#!/bin/bash

esptool.py --port /dev/ttyUSB0 erase_flash && \
    notify-send --urgency=low "hey you!" "Succes!" || \
    notify-send --urgency=critical "hey you!" "Oops"
