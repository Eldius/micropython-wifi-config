#!/bin/bash

function test {
rshell  << EOCMD
echo ""
echo "Connecting 01"
echo ""
connect serial /dev/ttyUSB0 115200
echo ""
echo "Connecting 02"
echo ""
connect serial /dev/ttyUSB1 115200
echo ""
echo "listing boards"
echo ""
boards
echo ""
echo "syncying files 01"
echo ""
rsync ./src/ /esp32-01/
echo ""
echo "syncying files 02"
echo ""
rsync ./src/ /esp8266-01/
echo ""
exit

EOCMD
}

function reset_device {
    #ampy -p $1 -b 115200 reset
    echo "Doing nothing..."
}

device_list=[]

for device in /dev/ttyUSB*
do
    echo "working on device: ${device}"
    reset_device ${device}
    sleep 10
    device_folder_name=$( rshell --quiet -p ${device} -b 115200 boards | awk '{print $1}' )
    reset_device ${device}
    sleep 10
    echo "device name: ${device_folder_name}"
    rshell -p ${device} -b 115200 rsync ./src/ /${device_folder_name}
done
