#!/bin/bash

#http://micropython.org/resources/firmware/esp8266-20180511-v1.9.4.bin
#http://micropython.org/resources/firmware/esp32-20190113-v1.9.4-779-g5064df207.bin
#https://micropython.org/resources/firmware/esp32-20190529-v1.11.bin
#https://micropython.org/resources/firmware/esp32-20190819-v1.11-223-g7d851a27f.bin


#mkdir -p bin && \
#    cd bin && \
#    wget https://micropython.org/resources/firmware/esp32-20190529-v1.11.bin && \
#    wget https://micropython.org/resources/firmware/esp32-20190819-v1.11-223-g7d851a27f.bin && \
#    notify-send --urgency=low "hey you!" "Succes!" || \
#    notify-send --urgency=critical "hey you!" "Oops"

mkdir -p bin && \
    cd bin && \
    wget http://micropython.org/resources/firmware/esp8266-20190529-v1.11.bin && \
    wget http://micropython.org/resources/firmware/esp8266-20190821-v1.11-231-g96ace8082.bin && \
    notify-send --urgency=low "hey you!" "Succes!" || \
    notify-send --urgency=critical "hey you!" "Oops"
