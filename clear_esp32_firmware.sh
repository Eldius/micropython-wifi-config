#!/bin/bash

esptool.py --chip esp32 erase_flash && \
    notify-send --urgency=low "hey you!" "Succes!" || \
    notify-send --urgency=critical "hey you!" "Oops"