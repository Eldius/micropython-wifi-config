#!/bin/bash

. .venv/bin/activate

esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash -z 0x1000 bin/esp32-20190819-v1.11-223-g7d851a27f.bin && \
    notify-send --urgency=low "hey you!" "Succes!" || \
    notify-send --urgency=critical "hey you!" "Oops"
