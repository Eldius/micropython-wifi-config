
config_str = "GET /192.168.4.1/config?ssid=[esp8266]%20Config%20Network&password=asdf HTTP/1.1"

def parse_query_string(query_str):
    from urllib import unquote_plus
    parameters = query_str.split('&')
    parsed_params = {}
    for param in parameters:
        tmp = param.split('=')
        parsed_params[tmp[0]] = unquote_plus(tmp[1])
    return parsed_params

def configure(config_str):
    print("[CONFIG] config_str: '%s'" % (config_str))
    query_str = (config_str.split('?')[1]).replace(' HTTP/1.1', '')
    print("[CONFIG] tmp: '%s'" % query_str)
    print(parse_query_string(query_str))

configure(config_str)
