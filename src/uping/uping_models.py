
class PingResponse(object):
    def __init__(self, host, addr, elapsed_time, packet_size, had_success = True):
        self.host = host
        self.addr = addr
        self.elapsed_time = elapsed_time
        self.packet_size = packet_size
        self.had_success = had_success
    def __str__():
        return "PingResponse:{\"host\": \"%s\", \"addr\": \"%s\", \"elapsed_time\": \"%s\", \"packet_size\": \"%s\", \"had_success\": \"%s\"}" % (host, addr, str(elapsed_time), str(packet_size), str(had_success))

class PingResult(object):
    def __init__(self, responses):
        self.responses = responses
    def __str__():
        response = ""
        for p in responses:
            response = response + "%s\n" % str(p)
