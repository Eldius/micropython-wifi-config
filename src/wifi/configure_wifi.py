
import board
import network
#from dns.captive import setup as setup_captive_portal

def start_ap_mode():
    import os
    ap_if = network.WLAN(network.AP_IF)
    ap_if.active(True)
    ap_if.config(essid="[%s] Config Network" % (os.uname().nodename), authmode=network.AUTH_OPEN)
    print(ap_if.ifconfig())
    #setup_captive_portal()

def configure(config_str):
    print("[CONFIG] config_str: %s" % (config_str))

def start_config_form(network_list):
    start_ap_mode()

    from wifi.form import config_form_content
    import socket
    addr = socket.getaddrinfo('0.0.0.0', 80)[0][-1]

    s = socket.socket()
    s.bind(addr)
    s.listen(1)

    print('listening on', addr)

    while True:
        cl, addr = s.accept()
        print('client connected from', addr)
        cl_file = cl.makefile('rwb', 0)
        while True:
            line = cl_file.readline()
            if '/config' in line.decode('utf-8'):
                configure(line.decode('utf-8'))
                return
            print("request_content: %s" % (line))
            #if not line or line == '':
            if not line or line == b'\r\n':
                break

        network_options = []
        for n in network_list:
            network_options.append("<option value=\"%s\">%s</option>" % (n, n))
        response = config_form_content() % '\n'.join(network_options)
        cl.send(response)
        cl.close()
        print("####################################")

def list_networks():
    sta_if = network.WLAN(network.STA_IF)
    sta_if.active(True)
    network_dict_list=sta_if.scan()
    network_list = []
    for n in network_dict_list:
        network_list.append(n[0].decode("utf-8") )
    sta_if.active(False)
    return network_list

def test_configuration():
    try:
        print("Trying to import configuration from filesystem...")
        import wifi_configuration as config
    except:
        print("Ooops! Can't open config module... Starting in AP mode...")
        start_config_form(list_networks())
